/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */
import { useBlockProps } from '@wordpress/block-editor';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
 import { Component } from '@wordpress/element';
 import { withSelect } from '@wordpress/data';
 import { compose } from '@wordpress/compose';
 import { Spinner } from '@wordpress/components';

 class Edit extends Component {
	render() {
	const { attributes, setAttributes, currentcat, currentpostcat } = this.props;
    // return a Spinner while the maps are loading
    if ( currentpostcat === null || currentcat === null || currentcat === undefined || ! Object.keys( currentcat).length > 0) {
      return (
        <div className="wp-block">
          <Spinner />
          { __( 'Loading Category', 'post-category-color' ) }
        </div>
      );
    }
	let categories = [];
	let postcategory = '';
	for(let i = 0; i < currentcat.length; i++){
		if(currentcat[i].id == currentpostcat[0]){
			postcategory = currentcat[i].name;
		}
	}
		let currentlowercat = postcategory.toLowerCase();
		let optionA = ['a','b','c','d','e','f','g','h','i','j','k','l','m'],
			optionB = ['n','o','p','q','r','s','t','u','v','w','x','y','z'],
			whichoption = "",
			bgcolor = "blue";

		if(optionA.includes(currentlowercat.charAt(0))){
			whichoption = "You are currently looking at Option A-M";
			bgcolor = "wp-block blue";
		}else if(optionB.includes(currentlowercat.charAt(0))){
			whichoption = "You are currently looking at Option N-Z";
			bgcolor = "wp-block green";
		}else{
			whichoption = "You have no category";
			bgcolor = "wp-block transparent";
		}
		attributes.whichoption = whichoption;
		attributes.bgcolor = bgcolor;
		return (
			<div className={bgcolor}>
				<p>{whichoption}</p>
			</div>
		);
	}
}
export default compose( [
	withSelect( ( select ) => {
	  return {
		currentpostcat : select('core/editor').getEditedPostAttribute('categories'),
		currentcat : select( 'core' ).getEntityRecords( 'taxonomy', 'category' )
	  };
	} ),
  ] )( Edit );